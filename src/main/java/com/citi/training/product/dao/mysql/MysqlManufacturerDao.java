package com.citi.training.product.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.product.dao.ManufacturerDao;
import com.citi.training.product.model.Manufacturer;
import com.citi.training.product.model.Product;

@Component
public class MysqlManufacturerDao implements ManufacturerDao {

		@Autowired
	    JdbcTemplate tpl;

	    public List<Manufacturer> findAll(){
	        return tpl.query("select id, name, address from manufacturer",
	                         new ManufacturerMapper());
	    }

	    @Override
	    public Manufacturer findById(int id) {
	        List<Manufacturer> Manufacturers = this.tpl.query(
	                "select id, name, price from manufacturers where id = ?",
	                new Object[]{id},
	                new ManufacturerMapper()
	        );
	        if(Manufacturers.size() <= 0) {
	            return null;
	        }
	        return Manufacturers.get(0);
	    }

	    @Override
	    public int create(Manufacturer Manufacturer) {
	        KeyHolder keyHolder = new GeneratedKeyHolder();
	        this.tpl.update(
	            new PreparedStatementCreator() {
	                @Override
	                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	                    PreparedStatement ps =
	                            connection.prepareStatement("insert into manufacturer (name, address) values (?, ?)",
	                            Statement.RETURN_GENERATED_KEYS);
	                    ps.setString(1, Manufacturer.getName());
	                    ps.setString(2, Manufacturer.getAddress());
	                    return ps;
	                }
	            },
	            keyHolder);
	        return keyHolder.getKey().intValue();
	    }

	    @Override
	    public void deleteById(int id) {
	        this.tpl.update("delete from manufacturer where id=?", id);
	    }

	    private static final class ManufacturerMapper implements RowMapper<Manufacturer> {
	        public Manufacturer mapRow(ResultSet rs, int rowNum) throws SQLException {
	            return new Manufacturer(rs.getInt("id"),
	                                rs.getString("name"),
	                                rs.getString("address"));
	        }
	    }
}
