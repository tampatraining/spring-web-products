package com.citi.training.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.product.dao.ManufacturerDao;
import com.citi.training.product.dao.ProductDao;
import com.citi.training.product.model.Manufacturer;
import com.citi.training.product.model.Product;

@Component
public class ManufacturerService {

	@Autowired
	ManufacturerDao manufacturerDao;
	
	public List<Manufacturer> findAll(){
		return manufacturerDao.findAll();
	}
		
	public int create(Manufacturer Manufacturer) {
		 return manufacturerDao.create(Manufacturer);
	 }
	public Manufacturer findById(int id) {
	    	return manufacturerDao.findById(id);
	 }
	 public void deleteById(int id) {
		 	manufacturerDao.deleteById(id);
	 }
	
	
}
