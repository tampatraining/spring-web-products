package com.citi.training.product.service;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.product.dao.ProductDao;
import com.citi.training.product.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTests {

	@Autowired
	private ProductService productService;
	
	@MockBean
	private ProductDao mockProductDao;
	
	@Test
	public void test_createRuns() {
		int newId = 1;
		
		when(mockProductDao.create(any(Product.class))).thenReturn(newId);
		
		Product otherProduct = new Product(2,"BreakTest",1000);
		Product testProduct = new Product(99,"Quinoa",300.00);
		
		
		int createId = productService.create(testProduct);
		
		verify(mockProductDao).create(otherProduct);
		
		assertEquals(newId, createId);
	}
	
	@Test
	public void test_deleteRuns() {
		
		int testId = 55;
		
		Product testProduct = new Product(55,"Coffee",100.99);
		
		when(mockProductDao.findById(testId)).thenReturn(testProduct);
		
		Product returnedProduct = productService.findById(testId);
		
		verify(mockProductDao).findById(testId);
		assertEquals(testProduct,returnedProduct);
		
	}
	
	@Test
	public void test_findAll() {
		Product testProduct = new Product(40,"Coffee",15.99);
		Product testProduct1 = new Product(41,"Coffee Bean",16.99);
		
		List<Product> productList = new ArrayList();
		
		productList.add(testProduct);
		productList.add(testProduct1);
		
		when(mockProductDao.findAll()).thenReturn(productList);
		
		List<Product> resultList = productService.findAll()	;
		
		assertEquals(resultList,productList);

		}
	
	@Test
	public void test_findById() {
		
		int tempId = 3;
		
		productService.findById(tempId);
		
		verify(mockProductDao).findById(tempId);
			
	}

}
